#!/bin/bash

###########################################
### Samuel Gerner			###
### 170216				###
### samuel.gerner@fh-campuswien.ac.at	###
###########################################

#Wrapper for the docker container 'gerners/16s-qiime:latest'
#

########################
#default configfile creation
configfile() {
echo -e "#################################################################
### Configfile for Metagenomics Taxonomy Workflow, created: $DATE
#################################################################
# To disable a parameter, use value 'False' (preferred over deletion of line)
# CAVEAT: Keep order of arguments, especially for tools: Trimmomatic, CheckFASTQ, Decontaminate
#################################################################
### Parameters #########
########################
Cutadapt:minlength              200
Cutadapt:maxlength              600
Cutadapt:minqual                20
Fastqc:noextract                True
Fastqc:threads                  32
Fastqc:quiet                    True
PEAR:threads                    32
Trimmomatic:jarfile             /opt/Trimmomatic-0.36/trimmomatic-0.36.jar
Trimmomatic:threads             32
Trimmomatic:leading             10
Trimmomatic:trailing            10
Trimmomatic:slidingwindow       4:15
Trimmomatic:minlength           70
CheckFASTQ:script               /454_BH/Scripts/CheckFASTQ_gzipped.pl
CheckFASTQ:mode                 replace
Qiime:threads                   32
Qiime:revStrandMatch            True
ChimeraCheck:database           /opt/databases/rdp_gold/rdp_gold.fa
ChimeraCheck:threads            32
##########################################################################
### Values for not self-explanatory Parameters ###########################
##########################################################################
#CheckFASTQ:mode        replace|delete (default replace if not set)
##########################################################################
### Mandatory flags for Dependencies #####################################
##########################################################################
#Trimmomatic:jarfile            /path/to/trimmomatic-x.xx.jar
#CheckFASTQ:script              /path/to/CheckFASTQ_gzipped.pl" > $OUTDIR/configfile
}

########################
#help message
usage() { echo -e "Usage: $0 -a <reads.sff> -o <Outputdir> -c <Configfile> -m <mode>\n
\t-a\tInput reads in sff format
\t-o\tOutput Directory (absolute Filepath, default current working directory)
\t-m\tMode (16S-pe|16S-se|16S-sff|test)
\t-c\tConfigfile (if value is 'default', default configfile will be created and used
\t\tCAVEAT: update config file for first time use or edit default paths in script
\t-k\t[OPTIONAL] Don't remove temporary folder with all the intermediated files [default:false])
\t-r\tOnly create default configfile and exit,
\t\ttogether with options -c and -o
\n\t-h\tPrints this help message\n
Example:
\t$0 -o /path/to/outdir -c <configfile>|default -a /path/to/reads.sff -m 16S-sff
\t$0 -r";
exit; }

########################
#parse command line options
while getopts ":a:b:c:o:m:rhk" opt; do
        case $opt in
                a)
                READSF=$OPTARG
                if [[ ! -r $READSF || ! -f  $READSF ]]; then
                >&2 echo -e "ERROR: SFF file not readable\n"; pwd; exit 1 ; fi
                ;;
                c)
                if [[ $OPTARG == "default" ]]; then
                        DEFAULTCONFIG="1"
                else
                        if [[ -r $OPTARG ]]; then
                                CONFIGF=$OPTARG
                        else
                                >&2 echo -e "ERROR: Configfile not readable\n"; exit 1;
                        fi
                fi
                ;;
                k)
                TMP_DEL="1"
                ;;
                o)
                OUTDIR=$OPTARG
                if [[ -d $OUTDIR ]]; then
                        echo -e "WARNING: Outputdirectory '$OUTDIR' already exists, results may be overwritten"
                else
                        mkdir $OUTDIR
                fi
                ;;
                m)
                MODERE='16S-pe|16S-se|test|16S-sff'
                if [[ $OPTARG =~ $MODERE ]]; then
                        MODE=$OPTARG
                else
                        >&2 echo "ERROR: Mode not recognized, please provide valid choice"
                        usage
                fi
                ;;
	        r)
                echo -e "Creating Configfile in current working directory...\n"
                OUTDIR="./"
                configfile
                exit;
                ;;
                h)
                usage
                ;;
                \?)
                >&2 echo -e "ERROR: Invalid option: -$OPTARG\n" >&2; usage
                ;;
                :)
                >&2 echo -e "ERROR: Option -$OPTARG requires an argument\n" >&2; usage
                ;;
        esac
done

########################
#if defaultconfigfile is selected, create default config
if [[ $DEFAULTCONFIG == "1" ]]; then
        configfile
        CONFIGF="$OUTDIR/configfile"
        echo "Created default configfile in '$OUTDIR'"
else
        cp $CONFIGF $OUTDIR/configfile
        CONFIGF="$OUTDIR/configfile"
fi

########################
#check if all Commandline Parameter are complete
MODEPE='16S-pe'
MODESE='16S-se'
SFFRE='sff$'

if ! [[ $CONFIGF ]]; then
        >&2 echo -e "ERROR: Please provide config file or use default config by setting '-c default'\n"
        usage
        exit 1;
elif ! [[ $OUTDIR ]]; then
        OUTDIR=$(pwd)
elif ! [[ $MODE ]]; then
        >&2 echo -e "ERROR: No mode provided, please provide valid choice via the -m option"
        usage
        exit 1;
elif [[ $MODE =~ $MODEPE  ]]; then
        if ! [[ $READSF && $READSR ]]; then
        >&2 echo -e "ERROR: Please provide both forward and reverse reads, check options with '-h'\n"
        exit 1;
        fi
elif [[ $MODE =~ $MODESE ]]; then
        if ! [[ $READSF ]]; then
        >&2 echo -e "ERROR: Please provide fasta reads file, check options with '-h'\n"
        exit 1;
        fi
#elif [[ $MODE == '16S-sff' ]]; then
elif [[ $MODE == 'test' ]]; then
        if ! [[ $READSF ]]; then
        >&2 echo -e "ERROR: Please provide fasta reads file, check options with '-h'\n"
        exit 1;
        fi

        if ! [[ $READSF =~ $SFFRE ]]; then
        >&2 echo -e "ERROR: No *.sff input file in sff mode, please provide correct input file\n"
        exit 1;
        fi
fi

########################
#if input file is not in $OUTDIR, create softlink to input file in $OUTDIR, as only $OUTDIR will be mounted into the docker container

#remove potential trailing '/' in directory path for proper comparison afterwards
if [[ "${OUTDIR:$((${#OUTDIR}-1)):1}" == "/" ]]; then
        OUTDIRT=${OUTDIR::-1}
else
        OUTDIRT=$OUTDIR
fi

#check if inputfile is in outputdir, if not, create link in outputdir to inputfile and reset input variable
if ! [[ $(dirname $READSF) == $OUTDIRT ]]; then
#        ln -s $READSF $OUTDIRT/$(basename $READSF)
	cp $READSF $OUTDIRT/$(basename $READSF)
	READSF=$(basename $READSF)
fi

DATE=$(date +"_d%y%m%d_t%H%M%S")

########################
#change potential relative dir path into absolute path to enable mounting by docker
ABSOUTDIR=$(cd $OUTDIR;pwd)

########################
#execute docker
docker run -u $(id -u):$(id -g) -v $ABSOUTDIR/:/workdata gerners/16s-qiime:latest -c configfile -o output${DATE} -m $MODE -a $(basename $READSF) | tee $OUTDIR/logfile${DATE}
